//
//  ConfigurationManager.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 31/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

@objcMembers class ConfigurationManager: NSObject {
    
    // MARK: - Properties -
    
    static let shared = ConfigurationManager()
    var configuration: Configuration?
    
    var imageBaseURL: String {
        return self.configuration?.images.secureBaseUrl ?? Constants.baseImageURL
    }
    
    var bigPosterSize: String {
        return self.configuration?.images.posterSizes.last ?? "original"
    }
    
    var bigBackdropSize: String {
        return self.configuration?.images.backdropSizes.last ?? "original"
    }
    
    // MARK: - Inits -
    
    func getConfiguration() {
        ServerManager.shared.getConfiguration { [unowned self](configuration, error) in
            self.configuration = configuration
        }
    }
    
}
