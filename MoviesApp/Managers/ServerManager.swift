//
//  ServerManager.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 29/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

@objc class ServerManager: NSObject {
    
    // MARK: - Properties -
    
    @objc public static let shared = ServerManager()
    private let baseURL = "https://api.themoviedb.org/3/"
    
    // MARK: - Public Methods -
    
    public func search(query: String, completion: @escaping (_ response: MultiSearchResponse?, _ error: Error?) -> Void) {
        let queryItems = [
            URLQueryItem(name: "api_key", value: Constants.apiKey),
            URLQueryItem(name: "query", value: query)
        ]
        
        self.request(path: "search/multi", queryItems: queryItems, completion: completion)
    }
    
    @objc public func getMovieDetails(movieId: Int, completion: @escaping (_ response: MovieDetails?, _ error: Error?) -> Void) {
        let queryItems = [
            URLQueryItem(name: "api_key", value: Constants.apiKey)
        ]
        
        self.request(path: "movie/\(movieId)", queryItems: queryItems, completion: completion)
    }
    
    @objc public func getTVShowDetails(tvShowId: Int, completion: @escaping (_ response: TVShowDetails?, _ error: Error?) -> Void) {
        let queryItems = [
            URLQueryItem(name: "api_key", value: Constants.apiKey)
        ]
        
        self.request(path: "tv/\(tvShowId)", queryItems: queryItems, completion: completion)
    }
    
    public func getConfiguration(completion: @escaping (_ response: Configuration?, _ error: Error?) -> Void) {
        let queryItems = [
            URLQueryItem(name: "api_key", value: Constants.apiKey)
        ]
        
        self.request(path: "configuration", queryItems: queryItems, completion: completion)
    }
    
    // MARK: - Private Methods -
    
    private func request<U: Decodable>(path: String, queryItems: [URLQueryItem], completion: @escaping (_ response: U?, _ error: Error?) -> Void) {
        let urlString = "\(baseURL)\(path)"
        
        var urlComponents = URLComponents(string: urlString)
        
        urlComponents?.queryItems = queryItems
        
        guard let url = urlComponents?.url else {
            print("Error: URL is not valid.")
            return
        }
        
        let defaultSession = URLSession(configuration: .default)
        
        let dataTask = defaultSession.dataTask(with: url) { (data, urlResponse, error) in
            if let data = data {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                
                do {
                    let response = try decoder.decode(U.self, from: data)
                    completion(response, nil)
                } catch {
                    completion(nil, error)
                }
            } else {
                completion(nil, error)
            }
        }
        
        dataTask.resume()
    }
    
}
