//
//  Constants.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 29/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

@objcMembers class Constants: NSObject {
    
    // MARK: - Properties -
    
    static let apiKey = "6b2e856adafcc7be98bdf0d8b076851c"
    static let baseImageURL = "https://image.tmdb.org/t/p/"
    
}
