//
//  ViewController.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 29/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import UIKit
import SDWebImage

class SearchVC: UITableViewController {
    
    // MARK: - Properties -
    
    let searchController = UISearchController(searchResultsController: nil)
    var searchResultItems = [SearchResultItem<Movie, TVShow>]()
    var selectedSearchResultItem: SearchResultItem<Movie, TVShow>?
    
    var filteredSearchResultItems: [SearchResultItem<Movie, TVShow>] {
        return self.searchResultItems.filter({
            switch $0 {
            case .person:
                return false
            default:
                return true
            }
        })
    }
    
    @IBAction func refreshControlAction(_ sender: UIRefreshControl) {
        if let query = self.searchController.searchBar.text {
            self.search(query: query)
        }
    }
    
    // MARK: - Life cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.searchBar.delegate = self
        
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController = self.searchController
            self.navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            self.tableView.tableHeaderView = self.searchController.searchBar
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let showDetailsVC = segue.destination as? ShowDetailsVC {
            if let selectedSearchResultItem = self.selectedSearchResultItem {
                switch selectedSearchResultItem {
                case .movie(let movie):
                    showDetailsVC.movieId = Int32(movie.id)
                case .tvShow(let tvShow):
                    showDetailsVC.tvShowId = Int32(tvShow.id)
                default:
                    break
                }
            }
        }
    }
    
    // MARK: - Methods -
    
    func search(query: String) {
        self.refreshControl?.beginRefreshing()
        
        ServerManager.shared.search(query: query) { [unowned self](response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            self.searchResultItems = response?.results ?? []
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.refreshControl?.endRefreshing()
            }
        }
    }
    
}

// MARK: - UITableViewDataSource -

extension SearchVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredSearchResultItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultItemCell", for: indexPath) as! SearchTVC
        
        let searchResultItem = self.filteredSearchResultItems[indexPath.row]
        
        switch searchResultItem {
        case .movie(let movie):
            if let posterPath = movie.posterPath {
                cell.posterImageView.sd_setImage(with: URL(string: "\(ConfigurationManager.shared.imageBaseURL)\(ConfigurationManager.shared.bigPosterSize)/\(posterPath)"))
            } else {
                cell.posterImageView.image = nil
            }
            
            cell.titleLabel?.text = movie.title
            cell.voteAverageLabel.text = String(movie.voteAverage)
            cell.releaseDateLabel?.text = movie.releaseDate
        case .tvShow(let tvShow):
            if let posterPath = tvShow.posterPath {
                cell.posterImageView.sd_setImage(with: URL(string: "\(ConfigurationManager.shared.imageBaseURL)\(ConfigurationManager.shared.bigPosterSize)/\(posterPath)"))
            } else {
                cell.posterImageView.image = nil
            }
            
            cell.titleLabel?.text = tvShow.name
            cell.voteAverageLabel.text = String(tvShow.voteAverage)
            cell.releaseDateLabel?.text = tvShow.firstAirDate
        case .person:
            break
        }
        
        return cell
    }
    
}

// MARK: - UITableViewDelegate -

extension SearchVC {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedSearchResultItem = self.filteredSearchResultItems[indexPath.row]
        self.performSegue(withIdentifier: "ShowDetails", sender: nil)
    }
    
}

// MARK: - UISearchBarDelegate -

extension SearchVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let query = self.searchController.searchBar.text {
            self.search(query: query)
        }
        self.searchController.dismiss(animated: true)
    }
    
}
