//
//  ShowDetailsVC.m
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 31/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

@import SDWebImage;

#import "ShowDetailsVC.h"
#import "MoviesApp-Swift.h"

@interface ShowDetailsVC ()

@end

@implementation ShowDetailsVC

#pragma mark - Life cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak ShowDetailsVC *weakSelf = self;
    
    self.titleLabel.text = nil;
    self.summaryLabel.text = nil;
    self.genreLabel.text = nil;
    
    if (self.movieId) {
        [self.activityIndicatorView startAnimating];
        
        [[ServerManager shared] getMovieDetailsWithMovieId:self.movieId
                                                completion:^(MovieDetails *movieDetails, NSError *error) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        if (movieDetails.backdropPath) {
                                                            NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@/%@", [ConfigurationManager shared].imageBaseURL, [ConfigurationManager shared].bigBackdropSize, movieDetails.backdropPath]];
                                                            [weakSelf.backdropImageView sd_setImageWithURL:url];
                                                        }
                                                        
                                                        weakSelf.title = movieDetails.title;
                                                        weakSelf.titleLabel.text = movieDetails.title;
                                                        weakSelf.summaryLabel.text = movieDetails.overview;
                                                        weakSelf.genreLabel.text = movieDetails.genres.firstObject.name;
                                                        
                                                        [weakSelf.activityIndicatorView stopAnimating];
                                                    });
                                                }];
        return;
    }
    
    if (self.tvShowId) {
        [self.activityIndicatorView startAnimating];
        
        [[ServerManager shared] getTVShowDetailsWithTvShowId:self.tvShowId
                                                  completion:^(TVShowDetails *tvShowDetails, NSError *error) {
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          if (tvShowDetails.backdropPath) {
                                                              NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@/%@", [ConfigurationManager shared].imageBaseURL, [ConfigurationManager shared].bigBackdropSize, tvShowDetails.backdropPath]];
                                                              [weakSelf.backdropImageView sd_setImageWithURL:url];
                                                          }
                                                          
                                                          weakSelf.title = tvShowDetails.name;
                                                          weakSelf.titleLabel.text = tvShowDetails.name;
                                                          weakSelf.summaryLabel.text = tvShowDetails.overview;
                                                          weakSelf.genreLabel.text = tvShowDetails.genres.firstObject.name;
                                                          
                                                          [weakSelf.activityIndicatorView stopAnimating];
                                                      });
                                                  }];
        return;
    }
}

@end
