//
//  SearchTVC.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 30/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import UIKit

class SearchTVC: UITableViewCell {
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var voteAverageLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    
}
