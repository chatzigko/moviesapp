//
//  ShowDetailsVC.h
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 31/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowDetailsVC : UIViewController

#pragma mark - IBOutlets -

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UIImageView *backdropImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;

#pragma mark - Properties -

@property (nonatomic, assign) int movieId;
@property (nonatomic, assign) int tvShowId;

@end
