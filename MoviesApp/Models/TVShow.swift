//
//  TVShow.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 30/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

class TVShow: Decodable {
    
    // MARK: - Properties -
    
    var posterPath: String?
    var popularity: Double
    var id: Int
    var overview: String
    var backdropPath: String?
    var voteAverage: Double
    var mediaType: String
    var firstAirDate: String
    var originCountry: [String]
    var genreIds: [Int]
    var originalLanguage: String
    var voteCount: Int
    var name: String
    var originalName: String
    
    // MARK: - Inits -
    
    init(posterPath: String?, popularity: Double, id: Int, overview: String, backdropPath: String?, voteAverage: Double, mediaType: String,
         firstAirDate: String, originCountry: [String], genreIds: [Int], originalLanguage: String, voteCount: Int, name: String, originalName: String) {
        self.posterPath = posterPath
        self.popularity = popularity
        self.id = id
        self.overview = overview
        self.backdropPath = backdropPath
        self.voteAverage = voteAverage
        self.mediaType = mediaType
        self.firstAirDate = firstAirDate
        self.originCountry = originCountry
        self.genreIds = genreIds
        self.originalLanguage = originalLanguage
        self.voteCount = voteCount
        self.name = name
        self.originalName = originalName
    }
    
}
