//
//  TVShowDetails.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 31/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

@objcMembers class TVShowDetails: NSObject, Decodable {
    
    // MARK: - Properties -
    
    var backdropPath: String?
    var episodeRunTime: [Int]
    var firstAirDate: String
    var genres: [Genre]
    var homepage: String
    var id: Int
    var inProduction: Bool
    var languages: [String]
    var lastAirDate: String
    var name: String
    var numberOfEpisodes: Int
    var numberOfSeasons: Int
    var originCountry: [String]
    var originalLanguage: String
    var originalName: String
    var overview: String
    var popularity: Double
    var posterPath: String?
    var status: String
    var type: String
    var voteAverage: Double
    var voteCount: Int
    
    // MARK: - Inits -
    
    init(backdropPath: String?, episodeRunTime: [Int], firstAirDate: String, genres: [Genre], homepage: String, id: Int, inProduction: Bool,
         languages: [String], lastAirDate: String, name: String, numberOfEpisodes: Int, numberOfSeasons: Int, originCountry: [String],
         originalLanguage: String, originalName: String, overview: String, popularity: Double, posterPath: String?, status: String,
         type: String, voteAverage: Double, voteCount: Int) {
        self.backdropPath = backdropPath
        self.episodeRunTime = episodeRunTime
        self.firstAirDate = firstAirDate
        self.genres = genres
        self.homepage = homepage
        self.id = id
        self.inProduction = inProduction
        self.languages = languages
        self.lastAirDate = lastAirDate
        self.name = name
        self.numberOfEpisodes = numberOfEpisodes
        self.numberOfSeasons = numberOfSeasons
        self.originCountry = originCountry
        self.originalLanguage = originalLanguage
        self.originalName = originalName
        self.overview = overview
        self.popularity = popularity
        self.posterPath = posterPath
        self.status = status
        self.type = type
        self.voteAverage = voteAverage
        self.voteCount = voteCount
    }
    
}
