//
//  MovieDetails.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 31/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

@objcMembers class MovieDetails: NSObject, Decodable {
    
    // MARK: - Properties -
    
    var adult: Bool
    var backdropPath: String?
    var budget: Int
    var genres: [Genre]
    var homepage: String?
    var id: Int
    var imdbId: String?
    var originalLanguage: String
    var originalTitle: String
    var overview: String?
    var popularity: Double
    var posterPath: String?
    var releaseDate: String
    var revenue: Int
    var runtime: Int?
    var status: String
    var tagline: String?
    var title: String
    var video: Bool
    var voteAverage: Double
    var voteCount: Int
    
    // MARK: - Inits -
    
    init(adult: Bool, backdropPath: String?, budget: Int, genres: [Genre], homepage: String?, id: Int, imdbId: String?, originalLanguage: String,
         originalTitle: String, overview: String?, popularity: Double, posterPath: String?, releaseDate: String, revenue: Int, runtime: Int?,
         status: String, tagline: String?, title: String, video: Bool, voteAverage: Double, voteCount: Int) {
        self.adult = adult
        self.backdropPath = backdropPath
        self.budget = budget
        self.genres = genres
        self.homepage = homepage
        self.id = id
        self.imdbId = imdbId
        self.originalLanguage = originalLanguage
        self.originalTitle = originalTitle
        self.overview = overview
        self.popularity = popularity
        self.posterPath = posterPath
        self.releaseDate = releaseDate
        self.revenue = revenue
        self.runtime = runtime
        self.status = status
        self.tagline = tagline
        self.tagline = tagline
        self.title = title
        self.video = video
        self.voteAverage = voteAverage
        self.voteCount = voteCount
    }
    
}
