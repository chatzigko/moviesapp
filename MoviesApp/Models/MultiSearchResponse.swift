//
//  MultiSearchResponse.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 29/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

class MultiSearchResponse: Decodable {
    
    // MARK: - Properties -
    
    var page: Int
    var results: [SearchResultItem<Movie, TVShow>]
    var totalResults: Int
    var totalPages: Int
    
    // MARK: - Inits -
    
    init(page: Int, results: [SearchResultItem<Movie, TVShow>], totalResults: Int, totalPages: Int) {
        self.page = page
        self.results = results
        self.totalResults = totalResults
        self.totalPages = totalPages
    }
    
}
