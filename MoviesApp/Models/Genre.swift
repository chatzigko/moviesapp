//
//  Genre.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 31/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

@objcMembers class Genre: NSObject, Decodable {
    
    // MARK: - Properties -
    
    var id: Int
    var name: String
    
    // MARK: - Inits -
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
}
