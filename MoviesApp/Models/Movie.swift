//
//  Movie.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 29/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

class Movie: Decodable {
    
    // MARK: - Properties -
    
    var posterPath: String?
    var adult: Bool
    var overview: String
    var releaseDate: String
    var originalTitle: String
    var genreIds: [Int]
    var id: Int
    var mediaType: String
    var originalLanguage: String
    var title: String
    var backdropPath: String?
    var popularity: Double
    var voteCount: Int
    var video: Bool
    var voteAverage: Double
    
    // MARK: - Inits -
    
    init(posterPath: String?, adult: Bool, overview: String, releaseDate: String, originalTitle: String, genreIds: [Int], id: Int,
         mediaType: String, originalLanguage: String, title: String, backdropPath: String?, popularity: Double, voteCount: Int,
         video: Bool, voteAverage: Double) {
        self.posterPath = posterPath
        self.adult = adult
        self.overview = overview
        self.releaseDate = releaseDate
        self.originalTitle = originalTitle
        self.genreIds = genreIds
        self.id = id
        self.mediaType = mediaType
        self.originalLanguage = originalLanguage
        self.title = title
        self.backdropPath = backdropPath
        self.popularity = popularity
        self.voteCount = voteCount
        self.video = video
        self.voteAverage = voteAverage
    }
    
}
