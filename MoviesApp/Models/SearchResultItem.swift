//
//  SearchResultItem.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 30/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

// MARK: - Enum -

enum SearchResultItem<T, U> {
    case movie(T)
    case tvShow(U)
    case person
}

// MARK: - Decodable -

extension SearchResultItem: Decodable where T: Decodable, U: Decodable {
    
    init(from decoder: Decoder) throws {
        if let value = try? T(from: decoder) {
            self = .movie(value)
        } else if let value = try? U(from: decoder) {
            self = .tvShow(value)
        } else {
            self = .person
        }
    }
    
}
