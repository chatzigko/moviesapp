//
//  Configuration.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 31/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

@objcMembers class Configuration: NSObject, Decodable {
    
    // MARK: - Properties -
    
    var images: Image
    var changeKeys: [String]
    
    // MARK: - Inits -
    
    init(images: Image, changeKeys: [String]) {
        self.images = images
        self.changeKeys = changeKeys
    }
    
}
