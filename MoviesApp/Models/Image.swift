//
//  Image.swift
//  MoviesApp
//
//  Created by Konstantinos Chatzigeorgiou on 31/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation

@objcMembers class Image: NSObject, Decodable {
    
    // MARK: - Properties -
    
    var baseUrl: String
    var secureBaseUrl: String
    var backdropSizes: [String]
    var logoSizes: [String]
    var posterSizes: [String]
    var profileSizes: [String]
    var stillSizes: [String]
    
    // MARK: - Inits -
    
    init(baseUrl: String, secureBaseUrl: String, backdropSizes: [String], logoSizes: [String], posterSizes: [String], profileSizes: [String], stillSizes: [String]) {
        self.baseUrl = baseUrl
        self.secureBaseUrl = secureBaseUrl
        self.backdropSizes = backdropSizes
        self.logoSizes = logoSizes
        self.posterSizes = posterSizes
        self.profileSizes = profileSizes
        self.stillSizes = stillSizes
    }
    
}
